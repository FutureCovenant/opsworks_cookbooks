directory '/var/www/html' do
  owner 'apache'
  group 'apache'
  mode '0755'
  action :create
end

availabilityzone = `curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`

mount '/var/www/html' do
  device "#{availabilityzone}.fs-e8d32521.efs.eu-west-1.amazonaws.com:/html/"
  fstype 'nfs4'
  options 'nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2'
  dump 0
  action   [:mount, :enable]
end
