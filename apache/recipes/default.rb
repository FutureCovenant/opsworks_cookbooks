# Install some stuff.
#["php-mysql"].each do |pkg|
#  package pkg do
#    action :install
#  end
#end

# Replace php.ini file.
template '/etc/php.ini' do
  source "php.ini.erb"
end

# Replace httpd.conf file.
template '/etc/httpd/conf/httpd.conf' do
  source "httpd.conf.erb"
end

# Replace notrace.conf file.
template '/etc/httpd/conf.d/notrace.conf' do
  source "notrace.conf.erb"
end

# Replace ssl.conf file.
template '/etc/httpd/conf.d/ssl.conf' do
  source "ssl.conf.erb"
end

# Replace php.conf file.
template '/etc/httpd/conf.d/php.conf' do
  source "php.conf.erb"
end

# Replace welcome.conf file.
template '/etc/httpd/conf.d/welcome.conf' do
  source "welcome.conf.erb"
end

# Replace deflate.conf file.
template '/etc/httpd/conf.d/deflate.conf' do
  source "deflate.conf.erb"
end
